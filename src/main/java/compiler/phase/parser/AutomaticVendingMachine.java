package compiler.phase.parser;

import compiler.enums.DefaultToken;
import compiler.exc.TheParserException;
import compiler.table.TableIdentifiers;
import compiler.table.token.Token;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Queue;
import java.util.Stack;

import static compiler.enums.DefaultToken.*;
import static compiler.enums.DefaultToken.TypeDefaultToken.NON_TERMINAL_SYMBOL;

/**
 * Магазинный автомат - синтаксический анализатор
 */
public class AutomaticVendingMachine {

    /**
     * Магазин магазинного автомата
     */
    private final Stack<DefaultToken> marketStack;
    private final TableIdentifiers tableIdentifiers;

    public AutomaticVendingMachine(TableIdentifiers tableIdentifiers) {
        this.marketStack = new Stack<>();
        this.tableIdentifiers = tableIdentifiers;
    }

    /**
     * Метод по обработке лексем - синтаксический анализ
     *
     * @param tableTokens - очередь с лексемами
     * @return если не падает, значит строка верна правилам
     */
    public boolean processingSyntaxAnalysis(Queue<Token> tableTokens) {
        if (tableTokens == null || tableTokens.isEmpty()) return false;
        marketStack.push(PROGRAM); // Начальное правило

        boolean isDeclaration = true;//объявление переменных
        while (!tableTokens.isEmpty()) {
            if (marketStack.isEmpty())
                throw new TheParserException("Ожидался конец файла");
            // Текущий символ входной цепочки
            Token inputToken = tableTokens.peek();
            // Символ, вынутый из стека
            DefaultToken stacksToken = marketStack.pop();

            //проверка идентификаторов
            if (inputToken.getTypeToken() == BEGIN)
                isDeclaration = false;
            if (inputToken.getTypeToken() == IDENTIFY) {
                if (isDeclaration) {
                    //Если идентификатор не повторялся, то добавляем в ТИ
                    if (!tableIdentifiers.isTableContains(inputToken)) {
                        tableIdentifiers.addInTable(inputToken);
                    } else throw new TheParserException("Повторное объявление идентификатора: " + inputToken);
                } else if (!tableIdentifiers.isTableContains(inputToken)) {
                    throw new TheParserException("Необъявленный идентификатор: " + inputToken);
                }
            }

            //Если магазинный символ является терминалом
            if (stacksToken.getType() != NON_TERMINAL_SYMBOL) {
                if (inputToken.getTypeToken() == stacksToken)
                    tableTokens.poll();
                else throw new TheParserException("Неверный входной символ: "
                        + inputToken + "ожидался " + stacksToken);
            } else {
                // Если в стеке нетерминальный символ ищем под него правило и помещаем в магазин
                var returnRules = GramRules.returnRules(stacksToken, inputToken.getTypeToken());
                if (returnRules != null) {
                    var necessaryRules = new ArrayList<>(returnRules);//развертываемые правила
                    if (!necessaryRules.isEmpty()) {
                        Collections.reverse(necessaryRules);
                        marketStack.addAll(necessaryRules);
                        necessaryRules.clear();
                    }
                } else throw new TheParserException("Нет подходящего правила для данного не-терминала "
                        + inputToken + " в разделе правил " + GramRules.returnExcRules(stacksToken));
            }
        }
        if (!marketStack.isEmpty())
            throw new TheParserException("Ожидался конец файла");
        return true;
    }
}