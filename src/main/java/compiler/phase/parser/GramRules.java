package compiler.phase.parser;

import compiler.enums.DefaultToken;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Правила всегда будут задаваться в ручную - переписыванием,
 * Таблица строится по след принципу(Перенимает построение обычной ГРАМОТНО построенной грамматики языка)
 * 1) - Первый ключ - это элемент магазинного стека
 * 2) - Значение состоит из таблицы возможных, проверяемых на входе ЛЕКСЕМ(ключи внутренней мапы),
 * в значениях у которых лежат варианты правил.
 */
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GramRules {
    private static final Map<DefaultToken, Map<DefaultToken, List<DefaultToken>>> ANALYSIS_TABLE = Map.of(
            DefaultToken.PROGRAM, Map.of(
                    DefaultToken.VAR, List.of(DefaultToken.DECLARING_VAR, DefaultToken.DESCRIPTION_CALCULATION)
            ),
            DefaultToken.DECLARING_VAR, Map.of(
                    DefaultToken.VAR, List.of(DefaultToken.VAR, DefaultToken.IDENTIFY, DefaultToken.VAR_LIST, DefaultToken.COLON, DefaultToken.INTEGER, DefaultToken.SEMICOLON)
            ),
            DefaultToken.VAR_LIST, Map.of(
                    DefaultToken.COMMA, List.of(DefaultToken.COMMA, DefaultToken.IDENTIFY, DefaultToken.VAR_LIST),
                    DefaultToken.COLON, Collections.emptyList(),
                    DefaultToken.BRACKET_RIGHT, Collections.emptyList()
            ),
            DefaultToken.DESCRIPTION_CALCULATION, Map.of(
                    DefaultToken.BEGIN, List.of(DefaultToken.BEGIN, DefaultToken.INSTRUCTION_LIST, DefaultToken.END)
            ),
            DefaultToken.INSTRUCTION_LIST, Map.of(
                    DefaultToken.IDENTIFY, List.of(DefaultToken.ASSIGMENT_LIST, DefaultToken.INSTRUCTION_LIST),
                    DefaultToken.READ, List.of(DefaultToken.READ, DefaultToken.BRACKET_LEFT, DefaultToken.IDENTIFY, DefaultToken.VAR_LIST, DefaultToken.BRACKET_RIGHT, DefaultToken.SEMICOLON, DefaultToken.INSTRUCTION_LIST),
                    DefaultToken.WRITE, List.of(DefaultToken.WRITE, DefaultToken.BRACKET_LEFT, DefaultToken.IDENTIFY, DefaultToken.VAR_LIST, DefaultToken.BRACKET_RIGHT, DefaultToken.SEMICOLON, DefaultToken.INSTRUCTION_LIST),
                    DefaultToken.FOR, List.of(DefaultToken.FOR, DefaultToken.IDENTIFY, DefaultToken.ASSIGMENT_MARK, DefaultToken.EXPRESSION, DefaultToken.TO, DefaultToken.EXPRESSION, DefaultToken.DO,
                            DefaultToken.INSTRUCTION_LIST, DefaultToken.END_FOR, DefaultToken.INSTRUCTION_LIST),
                    DefaultToken.END_FOR, Collections.emptyList(),
                    DefaultToken.END, Collections.emptyList()
            ),
            //Когда должен закончиться список присваиваний?
            //Когда встретиться один из операторов INSTRUCTION_LIST
            DefaultToken.ASSIGMENT_LIST, Map.of(
                    DefaultToken.IDENTIFY, List.of(DefaultToken.IDENTIFY, DefaultToken.ASSIGMENT_MARK, DefaultToken.EXPRESSION, DefaultToken.SEMICOLON, DefaultToken.ASSIGMENT_LIST),
                    DefaultToken.READ, Collections.emptyList(),
                    DefaultToken.WRITE, Collections.emptyList(),
                    DefaultToken.FOR, Collections.emptyList(),
                    DefaultToken.END_FOR, Collections.emptyList(),
                    DefaultToken.END, Collections.emptyList()
            ),
            DefaultToken.EXPRESSION, Map.of(
                    DefaultToken.MINUS, List.of(DefaultToken.MINUS, DefaultToken.EXPRESSION),//?
                    DefaultToken.BRACKET_LEFT, List.of(DefaultToken.BRACKET_LEFT, DefaultToken.EXPRESSION, DefaultToken.BRACKET_RIGHT, DefaultToken.SUBEXPRESSION),
                    DefaultToken.IDENTIFY, List.of(DefaultToken.IDENTIFY, DefaultToken.SUBEXPRESSION),
                    //TODO: может появится несколько минусов в ряд
                    DefaultToken.NUMERIC, List.of(DefaultToken.NUMERIC, DefaultToken.SUBEXPRESSION)
            ),
            /*EXPRESSION_NOT_MINUS, new HashMap(Map.of(
                    BRACKET_LEFT, new ArrayList<>(List.of(BRACKET_LEFT, EXPRESSION, BRACKET_RIGHT, SUBEXPRESSION)),
                    IDENTIFY, new ArrayList<>(List.of(IDENTIFY, SUBEXPRESSION)),
                    NUM, new ArrayList<>(List.of(NUM, SUBEXPRESSION))
            )),*/
            DefaultToken.SUBEXPRESSION, Map.of(
                    DefaultToken.SUMMATION, List.of(DefaultToken.SUMMATION, DefaultToken.EXPRESSION),
                    DefaultToken.MINUS, List.of(DefaultToken.MINUS, DefaultToken.EXPRESSION),
                    DefaultToken.MULTIPLICATION, List.of(DefaultToken.MULTIPLICATION, DefaultToken.EXPRESSION),
                    DefaultToken.SEMICOLON, Collections.emptyList(),
                    DefaultToken.BRACKET_RIGHT, Collections.emptyList(),
                    //TODO: выражений в условиях цикла может проскочить несколько
                    DefaultToken.TO, Collections.emptyList(),//так как подвыражение может встретиться в цикле
                    DefaultToken.DO, Collections.emptyList()
            )
    );

    /**
     * Метод возвращает правила, по которым происходит обработка.
     * Метод должен возвращать null, так как возврат пустой коллекции тоже правило
     *
     * @param stacksToken
     * @param inputToken
     * @return
     */
    public static List<DefaultToken> returnRules(DefaultToken stacksToken, DefaultToken inputToken) {
        if (ANALYSIS_TABLE.containsKey(stacksToken) &&
                ANALYSIS_TABLE.get(stacksToken).containsKey(inputToken))
            return ANALYSIS_TABLE.get(stacksToken).get(inputToken);
        else return null;
    }

    /**
     * Метод возвращает группу правил в которой произошла ошибка
     *
     * @param stacksToken
     * @return
     */
    public static Map<DefaultToken, List<DefaultToken>> returnExcRules(DefaultToken stacksToken) {
        return ANALYSIS_TABLE.getOrDefault(stacksToken, null);
    }
}