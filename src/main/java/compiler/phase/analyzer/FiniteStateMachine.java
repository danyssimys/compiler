package compiler.phase.analyzer;

import compiler.enums.State;
import compiler.exc.TokenAnalysisException;
import compiler.table.TableTokens;
import compiler.table.token.Token;

import static compiler.enums.DefaultToken.*;
import static compiler.enums.DefaultToken.TypeDefaultToken.KEYWORD;
import static compiler.enums.DefaultToken.TypeDefaultToken.TypeDefaultUnaryToken.DELIMITER;
import static compiler.enums.DefaultToken.TypeDefaultToken.TypeDefaultUnaryToken.OPERATION;
import static compiler.enums.State.*;

/**
 * Класс лексический анализатор. Разрабатывается в виде конечного автомата, со своими состояниями(матрицей перехода)
 * Анализатор будет идти по символам введенной строки, составлять и идентифицировать каждую лексему, добавляя ее в таблицу общих лексем
 */
public class FiniteStateMachine {

    private final TableTokens tableTokens;

    public FiniteStateMachine(TableTokens tableTokens) {
        this.tableTokens = tableTokens;
    }

    /**
     * Метод лексического анализа
     *
     * @param codeSymbols входная строка анализа
     * @return true в случае успешного разбора строки на лексемы
     */
    public boolean processingTokenAnalysis(String codeSymbols) {
        if (codeSymbols.trim().length() != 0) { //удаляет пробелы в начале и в конце
            int index = 0;
            var state = H;
            var unFilledTokenBd = new StringBuilder();

            boolean isFillToken = false;//участвует, когда первое условие не выполняется
            while (index < codeSymbols.length() || isFillToken) {
                switch (state) {
                    case H -> {
                        //Высчет лексемы и отправка в одно из состояний.
                        //Цикл формирует лексему посимвольно, пока не дойдет до разделителя
                        unFilledTokenBd.delete(0, unFilledTokenBd.length());
                        for (; index < codeSymbols.length(); index++) {
                            String eachSymbol = codeSymbols.substring(index, index + 1);
                            if (checkingDefaultToken(eachSymbol, DELIMITER)) {
                                var delimiterToken = getToken(eachSymbol, DELIMITER);
                                if (delimiterToken != WHITESPACE)//не добавляем пробел
                                    tableTokens.addInTable(Token.builder()
                                            .symbols(eachSymbol)
                                            .typeToken(delimiterToken)
                                            .build()
                                    );
                            } else {
                                unFilledTokenBd.append(eachSymbol);
                                //Если след символ - разделитель, мы заранее прерываем цикл и отправляем лексему дальше
                                if (((index + 1) < codeSymbols.length()) &&
                                        (checkingDefaultToken(
                                                codeSymbols.substring(index + 1, index + 2),
                                                DELIMITER))) {
                                    index++;
                                    break;
                                }
                            }
                        }
                        isFillToken = true;
                        //Проверка и выбор состояния
                        var unFilledToken = unFilledTokenBd.toString();
                        if (unFilledToken.matches("^[a-zA-Z_]{1,10}$")) state = ID;
                        else if (unFilledToken.matches("^(\\d)+$")) state = State.NUM;
                        else if (unFilledToken.matches("^([+\\-*=])$")) state = OP;
                        else throw new TokenAnalysisException("Token is doesnt belongs base tokens :"
                                    + unFilledTokenBd + "\n");
                    }
                    case ID -> {
                        var unFilledToken = unFilledTokenBd.toString();
                        var token = Token.builder()
                                .symbols(unFilledToken)
                                .build();
                        //проверка идентификатора на ключевое слово
                        if (checkingDefaultToken(unFilledToken, KEYWORD))
                            token.setTypeToken(getToken(unFilledToken, KEYWORD));
                        else {
                            token.setHashCode(unFilledToken.hashCode());
                            token.setTypeToken(IDENTIFY);
                        }
                        tableTokens.addInTable(token);
                        state = H;
                        isFillToken = false;
                    }
                    case NUM -> {
                        tableTokens.addInTable(Token.builder()
                                .symbols(unFilledTokenBd.toString())
                                .typeToken(NUMERIC)
                                .build()
                        );
                        state = H;
                        isFillToken = false;
                    }
                    case OP -> {
                        var unFilledToken = unFilledTokenBd.toString();
                        tableTokens.addInTable(Token.builder()
                                .symbols(unFilledToken)
                                .typeToken(getToken(unFilledToken, OPERATION))
                                .build()
                        );
                        state = H;
                        isFillToken = false;
                    }
                }
            }
        } else throw new TokenAnalysisException("Line is empty " + "\n");
        return true;
    }
}