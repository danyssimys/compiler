package compiler.table;

import compiler.table.token.Token;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Таблица лексем(только терминальных символов)
 */
public class TableTokens {

    private final Queue<Token> tableOfTokens = new LinkedList<>();
    private static final TableTokens INSTANCE = new TableTokens();

    private TableTokens() {
    }

    public static TableTokens getInstance() {
        return INSTANCE;
    }

    public Queue<Token> getTableOfTokens() {
        return new LinkedList<>(tableOfTokens);
    }

    public void addInTable(Token token) {
        tableOfTokens.add(token);
    }

    public void clear() {
        tableOfTokens.clear();
    }
}