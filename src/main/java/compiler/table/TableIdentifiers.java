package compiler.table;

import compiler.table.token.Token;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Класс содержит таблицу идентификаторов - лексем, являющихся переменными, либо константами.
 * Своеобразный СТЕК нашего компилятора, содержащий информацию о переменных и объектах в период выполнения программы.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TableIdentifiers {

    /**
     * Класс содержит информацию об идентификаторе
     */
    @Data
    @Builder
    private static class InformationAboutID<T> {
        private String typeVariable;
        private T value;
    }

    private static final TableIdentifiers INSTANCE = new TableIdentifiers();
    private final Map<Integer, InformationAboutID<?>> tableOfIdentifiers = new HashMap<>();

    public static TableIdentifiers getInstance() {
        return INSTANCE;
    }

    public Map<Integer, InformationAboutID<?>> getTableTokensOfIdentifiers() {
        return Collections.unmodifiableMap(tableOfIdentifiers);
    }

    public void addInTable(Token token) {
        tableOfIdentifiers.put(
                token.getHashCode(),
                InformationAboutID.builder()
                        .value(null)
                        .typeVariable(null)
                        .build()
        );
    }

    public boolean isTableContains(Token token) {
        return tableOfIdentifiers.containsKey(token.getHashCode());
    }

    public void clear() {
        tableOfIdentifiers.clear();
    }
}