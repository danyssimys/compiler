package compiler.table.token;

import compiler.enums.DefaultToken;
import lombok.Builder;
import lombok.Data;

/**
 * Класс содержит стандартный вид лексемы
 */
@Data
@Builder
public class Token {
    private int hashCode;//хеш код на информацию в таблице идентификаторов
    private DefaultToken typeToken;
    private String symbols;
}