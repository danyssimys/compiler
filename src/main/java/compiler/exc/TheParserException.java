package compiler.exc;

public class TheParserException extends RuntimeException {
    public TheParserException(String message) {
        super(message);
    }
}