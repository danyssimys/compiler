package compiler.exc;

public class TokenAnalysisException extends RuntimeException {
    public TokenAnalysisException(String message) {
        super(message);
    }
}