package compiler;

import compiler.phase.analyzer.FiniteStateMachine;
import compiler.phase.parser.AutomaticVendingMachine;
import compiler.table.TableIdentifiers;
import compiler.table.TableTokens;
import compiler.table.token.Token;

/**
 * Правила ввода строки:
 * 1) все лексемы пишутся через пробел(за исключением, если есть разделители(если нет, то синтаксический анализ их забракует))
 * 2) максимальная длина идентификатора 10 символов
 * 3) пробелы вначале и конце удаляются
 */
public class MainCompilerTest {
    public static void main(String[] args) {
        //TODO: доделать процессор - часть, исполняющая введенную программу
        //VAR a,b,i:INTEGER; BEGIN a = 4; b = 5; WRITE(b); b = 6; a = 8; READ(a,b,i);END
        FiniteStateMachine machine = new FiniteStateMachine(TableTokens.getInstance());
        machine.processingTokenAnalysis(
                "VAR a,b,i:INTEGER; BEGIN a = 4; b = 5; WRITE(b); b = 6 + a * a + b; a = 8; READ(a,b,i);END"
        );

        for (Token token : TableTokens.getInstance().getTableOfTokens()) {
            System.out.println(token.toString());
        }
        System.out.println("Разбор строки прошел успешно\n");

        AutomaticVendingMachine automaticVendingMachine = new AutomaticVendingMachine(TableIdentifiers.getInstance());
        if (automaticVendingMachine.processingSyntaxAnalysis(
                TableTokens.getInstance().getTableOfTokens())
        ) {
            System.out.println("Строка удовлетворяет грамматике\n");
        }
    }
}