package compiler.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

import static compiler.enums.DefaultToken.TypeDefaultToken.*;
import static compiler.enums.DefaultToken.TypeDefaultToken.TypeDefaultUnaryToken.DELIMITER;
import static compiler.enums.DefaultToken.TypeDefaultToken.TypeDefaultUnaryToken.OPERATION;

/**
 * Определенные языком шаблоны лексем
 */
@Getter
@AllArgsConstructor
public enum DefaultToken {

    IDENTIFY(PENDING_VARIABLE), NUMERIC(PENDING_VARIABLE),

    //Keywords
    VAR(KEYWORD), INTEGER(KEYWORD), READ(KEYWORD),
    FOR(KEYWORD), TO(KEYWORD), DO(KEYWORD),
    END_FOR(KEYWORD), WRITE(KEYWORD), BEGIN(KEYWORD), END(KEYWORD),

    //Non terminal symbols
    //Нетерминальные символы участвующие в грамматике,
    //из которых в последствии происходит развертка по правилам в терминальные символы
    PROGRAM(NON_TERMINAL_SYMBOL),
    DECLARING_VAR(NON_TERMINAL_SYMBOL),
    DESCRIPTION_CALCULATION(NON_TERMINAL_SYMBOL),
    VAR_LIST(NON_TERMINAL_SYMBOL),
    INSTRUCTION_LIST(NON_TERMINAL_SYMBOL),
    ASSIGMENT_LIST(NON_TERMINAL_SYMBOL),
    EXPRESSION(NON_TERMINAL_SYMBOL),
    SUBEXPRESSION(NON_TERMINAL_SYMBOL),
    EXPRESSION_NOT_MINUS(NON_TERMINAL_SYMBOL),

    //Delimiters
    SEMICOLON(DELIMITER, ";"),
    COLON(DELIMITER, ":"),
    COMMA(DELIMITER, ","),
    BRACKET_LEFT(DELIMITER, "("),
    BRACKET_RIGHT(DELIMITER, ")"),
    WHITESPACE(DELIMITER, " "),

    //Operations
    SUMMATION(OPERATION, "+"),
    MINUS(OPERATION, "-"), //минус играет роль как вычитания, так и унарного знака
    MULTIPLICATION(OPERATION, "*"),
    ASSIGMENT_MARK(OPERATION, "=");
    //ASSIGMENT_MARK(":="),
    //EQUALLY("=");

    /**
     * Типы стандартных лексем
     */
    public enum TypeDefaultToken {
        PENDING_VARIABLE, KEYWORD, NON_TERMINAL_SYMBOL;

        public enum TypeDefaultUnaryToken {
            DELIMITER, OPERATION
        }
    }

    private final Enum<?> type;
    private String symbol;

    DefaultToken(Enum<?> type) {
        this.type = type;
    }

    /**
     * Метод проверяет, является ли входящая строка одной из базовых унарных лексем
     *
     * @param symbol входная лексема
     * @param type   тип лексемы
     */
    public static boolean checkingDefaultToken(String symbol, TypeDefaultUnaryToken type) {
        return Arrays.stream(DefaultToken.values())
                .filter(x -> x.type == type)
                .anyMatch(x -> x.getSymbol().equals(symbol));
    }

    /**
     * Метод проверяет, является ли входящая строка одной из базовых лексем
     *
     * @param symbol входная лексема
     * @param type   тип лексемы
     */
    public static boolean checkingDefaultToken(String symbol, TypeDefaultToken type) {
        return Arrays.stream(DefaultToken.values())
                .filter(x -> x.type == type)
                .anyMatch(x -> x.name().equals(symbol));
    }

    /**
     * Метод возвращает унарную лексему по входящей строке
     *
     * @param symbol входная лексема
     * @param type   тип лексемы
     */
    public static DefaultToken getToken(String symbol, TypeDefaultUnaryToken type) {
        return Arrays.stream(DefaultToken.values())
                .filter(x -> x.type == type)
                .filter(x -> x.symbol.equals(symbol))
                .findFirst()
                .orElse(null);
    }

    /**
     * Метод возвращает лексему по входящей строке
     *
     * @param symbol входная лексема
     * @param type   тип лексемы
     */
    public static DefaultToken getToken(String symbol, TypeDefaultToken type) {
        return Arrays.stream(DefaultToken.values())
                .filter(x -> x.type == type)
                .filter(x -> x.name().equals(symbol))
                .findFirst()
                .orElse(null);
    }
}