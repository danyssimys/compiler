package compiler.phase.parser;

import compiler.exc.TheParserException;
import compiler.table.TableIdentifiers;
import compiler.table.TableTokens;
import compiler.phase.analyzer.FiniteStateMachine;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AutomaticVendingMachineTest {

    private FiniteStateMachine machine;
    private AutomaticVendingMachine automaticVendingMachine;
    private final TableTokens tableTokens = TableTokens.getInstance();
    private final TableIdentifiers tableIdentifiers = TableIdentifiers.getInstance();
    
    @BeforeEach
    void clearTable() {
        tableTokens.clear();
        tableIdentifiers.clear();
        machine = new FiniteStateMachine(tableTokens);
        automaticVendingMachine = new AutomaticVendingMachine(tableIdentifiers);
    }

    @Test
    @DisplayName("Тестирование корректной строки 1")
    void testProcessingParserRulesTrue1() {
        machine.processingTokenAnalysis("VAR a,b,i:INTEGER; BEGIN a = 4; b = 5; WRITE(b); b = 6 * a; READ(a,b,i);END");
        var processedTableToken = tableTokens.getTableOfTokens();
        assertTrue(automaticVendingMachine.processingSyntaxAnalysis(processedTableToken));
    }

    @Test
    @DisplayName("Тестирование корректной строки 2")
    void testProcessingParserRulesTrue2() {
        machine.processingTokenAnalysis("VAR a,b,i:INTEGER; BEGIN FOR b = 4 TO 5 DO  b = 6 * a; READ(a,b,i); WRITE(b); END_FOR END");
        var processedTableToken = tableTokens.getTableOfTokens();
        assertTrue(automaticVendingMachine.processingSyntaxAnalysis(processedTableToken));
    }

    @Test
    @DisplayName("Повторная инициализация")
    void testProcessingParserIDENTRepeatException() {
        machine.processingTokenAnalysis("VAR a,b,i,a:INTEGER; BEGIN a = 4; b = 5; WRITE(b); b = 6; a = 8; READ(a,b,i);END");
        var processedTableToken = tableTokens.getTableOfTokens();
        System.out.println(assertThrows(TheParserException.class, () ->
                automaticVendingMachine.processingSyntaxAnalysis(processedTableToken)).getMessage());
    }

    @Test
    @DisplayName("Необъявленная переменная")
    void testProcessingParserIDENTUndeclaredException() {
        machine.processingTokenAnalysis("VAR a,b,i:INTEGER; BEGIN a = 4; b = 5; WRITE(b); O = 6; a = 8; READ(a,b,i);END");
        var processedTableToken = tableTokens.getTableOfTokens();
        System.out.println(assertThrows(TheParserException.class, () ->
                automaticVendingMachine.processingSyntaxAnalysis(processedTableToken)).getMessage());
    }

    @Test
    @DisplayName("Неверный входной символ")
    void testProcessingParserRulesNotObserved1() {
        machine.processingTokenAnalysis("VAR a,b,i:INTEGER;BEGIN a 4 =; b = 5; WRITE b)(; b = 6; a = 8; READ(a,b,i);END");
        var processedTableToken = tableTokens.getTableOfTokens();
        System.out.println(assertThrows(TheParserException.class, () ->
                automaticVendingMachine.processingSyntaxAnalysis(processedTableToken)).getMessage());
    }

    @Test
    @DisplayName("Отсутствие правила")
    void testProcessingParserRulesDoesNotExist1() {
        machine.processingTokenAnalysis("VAR a,b,i:INTEGER; BEGIN 4 ; b = 5; WRITE(b); b = 6; a = 8; READ(a,b,i);END");
        var processedTableToken = tableTokens.getTableOfTokens();
        System.out.println(assertThrows(TheParserException.class, () ->
                automaticVendingMachine.processingSyntaxAnalysis(processedTableToken)).getMessage());
    }

    @Test
    @DisplayName("Отсутствие правила 2")
    void testProcessingParserRulesDoesNotExist2() {
        machine.processingTokenAnalysis("VAR a,b,i:INTEGER; BEGIN FOR b = 4 TO 5 END_FOR DO  b = 6 * a;  END");
        var processedTableToken = tableTokens.getTableOfTokens();
        System.out.println(assertThrows(TheParserException.class, () ->
                automaticVendingMachine.processingSyntaxAnalysis(processedTableToken)).getMessage());
    }
}