package compiler.phase.analyzer;

import compiler.exc.TokenAnalysisException;
import compiler.table.TableTokens;
import compiler.table.TableIdentifiers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Класс тестирования конечного автомата.
 * Протестировать можно только входящую строку по правилам, тк падений методов внутренних классов здесь нет
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FiniteStateMachineTest {

    private FiniteStateMachine machine;
    private final TableTokens tableTokens = TableTokens.getInstance();

    @BeforeAll
    void makeMachine() {
        machine = new FiniteStateMachine(tableTokens);
    }

    @BeforeEach
    void clearTable() {
        tableTokens.clear();
    }

    @Test
    void testProcessingTokenAnalysisNull() {//слипшиеся знаки операций
        System.out.println(assertThrows(TokenAnalysisException.class, () -> machine.processingTokenAnalysis(""))
                .getMessage()
        );
    }

    @Test
    void testProcessingTokenAnalysisOperationException() {//слипшиеся знаки операций
        System.out.println(assertThrows(TokenAnalysisException.class, () -> machine.processingTokenAnalysis(
                        "VAR a,b,i:INTEGER; BEGIN a=4; b=5; WRITE(b); b = 6; a=8; READ(a,b,i);END"
                )
        ).getMessage());
    }

    @Test
    void testProcessingTokenAnalysisIDENTException() {//большая длина переменной
        System.out.println(assertThrows(TokenAnalysisException.class, () -> machine.processingTokenAnalysis(
                        "VAR asdgsdGHDFHERE,b,i:INTEGER; BEGIN a = 4; b = 5; WRITE(b); b = 6; a = 8; READ(a,b,i);END"
                )
        ).getMessage());
    }

    @Test
    void testProcessingTokenAnalysisTrue() {
        assertTrue(machine.processingTokenAnalysis(
                        "VAR a,b,i:INTEGER; BEGIN a = 4; b = 5; WRITE(b); b = 6; a = 8; READ(a,b,i);END"
                )
        );
    }
}